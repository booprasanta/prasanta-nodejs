// let square = (x) =>{
// 	let result = x*x;
// 	return result;
// }
let square = x => x*x;

let user = {
	name : 'Andrew',
	sayHi: () =>{
		console.log(arguments);//Goes into global scope
		console.log(`Hi. I'm ${this.name}`); //this is binded to curent class in => function
	},
	sayHiAlt(){
		console.log(arguments);//Goes into local function scope
		console.log(`Hi. I'm ${this.name}`); //this is binded to curent object in general function
	}
};

user.sayHi();
user.sayHiAlt(1,2,3,4);