// console.log('inside notes.js');

//in module exports {} is the json object that gets exported when require('file_name') is used

// module.exports.age = 25;

const fs = require('fs');

let fetchNotes = ()=>{
    try {
        let notesString = fs.readFileSync('notes-json.json');
        return JSON.parse(notesString);
    } catch (e){
        return [];
    }
    
}

let saveNotes = (notes)=>{
    fs.writeFileSync('notes-json.json',JSON.stringify(notes));
}


let add = (title,description)=>{
    var notes = fetchNotes();
    var note = {
    	title,
    	description
    };
    var duplicateNotes = notes.filter(prevnote=>prevnote.title===note.title);

    if(duplicateNotes.length === 0){
		    notes.push(note);
            saveNotes(notes);
            return note;
    } else {
    	console.log("Duplicate Found");
    }

}

let getAll = ()=>{
    return fetchNotes();
}

let remove = (title)=>{
    let notes = fetchNotes();
    let filteredNotes = notes.filter(note=>note.title!==title);
    saveNotes(filteredNotes);
    return notes.length !== filteredNotes.length;
}

let getNote = (title)=>{
    let notes = fetchNotes();
    return notes.filter(obj=>obj.title===title).length>0 ? notes[0] : null;
}

let logNote = (note)=>{
    debugger;
    console.log("--");
	console.log(`Notes:${ note.title }`);
	console.log(`Body:${ note.description }`);
}

module.exports = {
	add,
	getAll,
    remove,
    getNote,
    logNote
}