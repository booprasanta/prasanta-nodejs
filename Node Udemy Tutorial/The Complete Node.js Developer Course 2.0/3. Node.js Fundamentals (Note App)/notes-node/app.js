console.log('Strting app.');

const fs = require('fs');/*Just pass one string*/
const os = require('os');
const notes = require('./notes.js');//Only requiring this is going to load the file
const _ = require('lodash');
const yargs = require('yargs');

// var command = process.argv;
var yargsArg = yargs.command('add','Add items to the list',{
	title : {
		description : 'Notes is required',
		demand : true,
		alias : 't'
	},
	description : {
		description : 'Notes body is required',
		demand : true,
		alias : 'd'	
	}
}).command('list','list all notes').command('read','list all notes',{
	title:{
		description : 'Notes Read is required',
		demand : true,
		alias : 't'	
	}
}).help().argv;
var command = yargsArg._;



switch(command[0]){
	case 'add':
		let note = notes.add(yargsArg.title,yargsArg.description);
		if(note){
			notes.logNote(note);
		}else{
			console.log('Failed');
		}
		break;
	case 'list':
		let allNotes = notes.getAll();
		console.log(`Printing ${allNotes.length} note(s).`)
		allNotes.forEach((noteObj)=>{
			notes.logNote(noteObj);
		});
		break;
	case 'read':
		let foundnote = notes.getNote(yargsArg.title);
		if(foundnote){
			console.log('Note Found');notes.logNote(foundnote);
		} else {
			console.log('Note Not Found');
		}
		break;
	case 'remove':
		let noteRemoved = notes.remove(yargsArg.title);
		let message = noteRemoved ? 'Note removed' : 'Note didn\'t remove';
		console.log(message);
		break;		
	default:
		console.log('command not found');
}



// console.log(_.isString(true));
// console.log(_.isString('Abc'));

// var filterRepeatations = _.uniq(['Abc',1,2,'Abc',1,2,3]);
// console.log(filterRepeatations);

// var user = os.userInfo();
// console.log(user);
// fs.appendFileSync('greetings.txt','Hello '+user.username+`! You are ${notes.age}`);
// console.log(`Hello ${user.username}`);/* Template String */