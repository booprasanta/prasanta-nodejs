const utils = require('./utils');
it('should add two numbers' , ()=>{
    /* Checks whether add works as it promises */
    var res = utils.add(33,11);
    // We don't assert anything to do with output 
    // throw new Error('Value Not Correct');

    if(res !==44){
        throw new Error(`Expected 44, but got ${res}`)
    }
} );/* Behaviour driven development */

it('should square a number',()=>{
    var res = utils.square(3);
    if(res!=9){
       throw new Error(`Square of 3 should be 9 but got ${res}`);
    }
})